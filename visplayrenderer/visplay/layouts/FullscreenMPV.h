/*
 *   This file is part of Visplay.
 *
 *   Visplay is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef FULLSCREENMPV_H
#define FULLSCREENMPV_H

#include "mpvwidget.h"

#include "visplay/layouts/BaseLayout.h"

#include <QApplication>
#include <QObject>
#include <QJsonObject>
#include <QVBoxLayout>
#include <QWebView>




namespace visplay::layouts {

class FullscreenMPV : public BaseLayout
{
    Q_OBJECT

    public:
        FullscreenMPV(QJsonObject& obj);
        ~FullscreenMPV();

        void display();

        MpvWidget                   *mpv_widget;
        QString                      path;
};

} // end namespace visplay::layouts
#endif // FULLSCREENMPV_H
