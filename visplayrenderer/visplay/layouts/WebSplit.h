/*
 *   This file is part of Visplay.
 *
 *   Visplay is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef WEBSPLIT_H
#define WEBSPLIT_H

#include "mpvwidget.h"

#include "visplay/layouts/BaseLayout.h"

#include <QApplication>
#include <QObject>
#include <QVBoxLayout>
#include <QWebView>
#include <QJsonObject>




namespace visplay::layouts {

class WebSplit : public BaseLayout
{
    Q_OBJECT

    public:
        WebSplit(QJsonObject& obj);
        ~WebSplit();

        void display();

        MpvWidget                   *mpv_widget;
        QWebView                    *web_view;

        QString path;
        QString url;
};

} // end namespace visplay::layouts
#endif // WEBSPLIT_H
