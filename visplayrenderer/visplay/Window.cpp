/*
 *   This file is part of Visplay.
 *
 *   Visplay is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "visplay/Window.h"
#include "visplay/layouts/DefaultLayout.h"
#include "visplay/layouts/FullscreenMPV.h"
#include "visplay/layouts/WebSplit.h"
#include "visplay/layouts/Identifiers.h"
#include "mpvwidget.h"

#include <QString>
#include <QDebug>
#include <QDialog>
#include <QLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QFileDialog>
#include <QtGui/QOpenGLContext>
#include <QUrl>

#include <QJsonDocument>
#include <QJsonObject>

#include <QWebView>

#include <unistd.h>
#include <mpv/client.h>

namespace visplay
{

Window::Window(int argc, char *argv[])
{

    playback_idle                  = true;

    win                            = new QWidget();

    layouts::DefaultLayout* default_layout = new layouts::DefaultLayout("Welcome to Visplay!");
    win->setLayout(default_layout->layout);
    win->show();
    /*
    QObject::connect(current_layout->mpv_widget, SIGNAL(playback_idle()),
                     this,       SLOT(mark_idle()));
    */
}

Window::~Window() {

}

void Window::open_media(QHttpEngine::Socket *httpSocket)
{
    QJsonDocument doc;
    QJsonObject obj;
    QString path;

    if (httpSocket->method() != QHttpEngine::Socket::POST) {
        send_text_reply(httpSocket, QHttpEngine::Socket::Forbidden, "/open_media only supports POST.\n");
        return;
    }

    httpSocket->readJson(doc);
    obj = doc.object();
    path = obj["path"].toString();
    qDebug() << "Path: " << path;

    mpv_widget->command(QStringList() << "loadfile" << path);

    playback_idle = false;

    send_text_reply(httpSocket, QHttpEngine::Socket::OK, "Playing requested media. \n");
}

void Window::get_idle_status(QHttpEngine::Socket *httpSocket)
{
    QString idle_string = QString::number(playback_idle) + "\n";
    QByteArray idle_byte_array = idle_string.toUtf8();

    if (httpSocket->method() != QHttpEngine::Socket::GET) {
        send_text_reply(httpSocket, QHttpEngine::Socket::Forbidden, "/get_idle_status only supports GET.\n");
        return;
    }

    send_text_reply(httpSocket, QHttpEngine::Socket::OK, idle_byte_array);
}

void Window::mark_idle()
{
    playback_idle = true;
}

void Window::get_property(QHttpEngine::Socket *httpSocket)
{
    QJsonDocument doc;
    QJsonObject   obj;
    QString       property_name;
    QVariant      property_value;
    QString       outgoing_string;


    if (httpSocket->method() != QHttpEngine::Socket::POST) {
        send_text_reply(httpSocket, QHttpEngine::Socket::Forbidden, "/get_property only supports POST.\n");
        return;
    }


    httpSocket->readJson(doc);
    obj = doc.object();
    property_name = obj["property"].toString();

    qDebug() << "property_name: " << property_name;

    property_value = mpv_widget->getProperty(property_name);

    qDebug() << "property_value: " << property_value;

    if (property_value.toString() == "") {
        outgoing_string = "ERROR";
    } else {
        outgoing_string = property_value.toString();
    }

    send_text_reply(httpSocket, QHttpEngine::Socket::OK, outgoing_string.toUtf8() + "\n");
}

void Window::swap_buffers(QHttpEngine::Socket *httpSocket)
{
    QWidget().setLayout(win->layout());
    win->setLayout(back_buffer->layout);
    win->show();
    back_buffer->display();


    send_text_reply(httpSocket, QHttpEngine::Socket::OK, "Buffers swapped.\n");

}

void Window::backbuffer_change_layout(QHttpEngine::Socket *httpSocket)
{
    QJsonDocument doc;
    QJsonObject obj;
    int new_layout_number;

    if (httpSocket->method() != QHttpEngine::Socket::POST) {
        send_text_reply(httpSocket, QHttpEngine::Socket::Forbidden, "/backbuffer/change_layout only supports POST.\n");
        return;
    }

    httpSocket->readJson(doc);
    obj = doc.object();
    new_layout_number = obj["layout"].toInt();
    back_buffer_layout_number = new_layout_number;
    qDebug() << "BackBuffer set to layout " << new_layout_number;

    switch (layouts::Identifiers(new_layout_number))
    {
        case layouts::Identifiers::DefaultLayout:
            back_buffer = new visplay::layouts::DefaultLayout(obj);
            break;
        case layouts::Identifiers::FullscreenMPV:
            back_buffer = new visplay::layouts::FullscreenMPV(obj);
            break;
        case layouts::Identifiers::WebSplit:
            back_buffer = new visplay::layouts::WebSplit(obj);
            break;
        default:
            break;
    }

    send_text_reply(httpSocket, QHttpEngine::Socket::OK, "Backbuffer set to layout.\n");
}

void Window::send_text_reply(QHttpEngine::Socket *http_socket, int status_code, const QByteArray& output_string)
{
    http_socket->setStatusCode(status_code);
    http_socket->setHeader("Content-Type", "text/plain");
    http_socket->writeHeaders();
    http_socket->write(output_string);
    http_socket->close();

}

} // end namespace visplay
