#    This file is part of Visplay.
#
#    Visplay is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Visplay is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Visplay.  If not, see <https://www.gnu.org/licenses/>.

import uri
import yaml


def get_sources_list(source_stream):
    from visplay.sources import source_constructors as sc
    all_sources = []

    sources_yaml = yaml.load(source_stream)
    source_stream.seek(0)

    # Handle imports
    if 'import' in sources_yaml and sources_yaml['import']:
        for name in sources_yaml['import']:
            source = sources_yaml['import'][name]
            path = uri.URI(source)
            new_source = sc[str(path.scheme)](name, path, is_import=True)
            all_sources.append(new_source)

    if 'add' in sources_yaml and sources_yaml['add']:
        # calls the corresponding source constructor (LocalSource/HTTPSource)
        # with the arguments provided, then appends to the list
        for source in sources_yaml['add']:
            path = uri.URI(source)
            new_source = sc[str(path.scheme)](source, path)
            all_sources.append(new_source)

    return all_sources


# Create the necessary namespaces
def sources_to_asset(name, sources):
    assets = {}
    for source in sources:
        for asset in source.assets:
            if type(source.assets[asset]) is list:
                source_asset = source.assets[asset]
                source_asset[:] = [name + ":" + item for item in source_asset]
            assets[name + ":" + asset] = source.assets[asset]
    return assets


def sources_to_play(name, sources):
    playlists = []
    for source in sources:
        if source.playlists:
            for asset in source.playlists:
                playlists.append(name + ":" + asset)
    return playlists
