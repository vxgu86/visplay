import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';

import AssetList from './AssetList';
import AssetEdit from './AssetEdit';


/**
 * Main tab container
 */
class AssetTab extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    assets: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
  }

  state = {
    editorOpen: false,
  }

  on_addButton_click() {
    this.setState({ editorOpen: true });
  }

  on_AssetEdit_close() {
    this.setState({ editorOpen: false });
  }

  render() {
    const { assets, loading } = this.props;
    const { editorOpen } = this.state;

    return (
      <div>
        <AssetList assets={assets} loading={loading}
          on_addButton_click={this.on_addButton_click.bind(this)} />
        <AssetEdit open={editorOpen}
          on_close={this.on_AssetEdit_close.bind(this)} />
      </div>
    );
  }
}

export default withStyles(() => {
  return {};
})(AssetTab);
