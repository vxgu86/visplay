from bottle import route, static_file


@route('/')
def index():
    return dist('index.html')


@route('/<filename:path>')
def dist(filename):
    return static_file(filename, root='dist')
