import json
import os

import yaml

from bottle import response, route
from visplay.config import Config


@route('/assets')
def assets():
    response.content_type = 'application/json'

    assets_file = os.path.join(Config._config_folder, 'assets.yaml')

    with open(assets_file) as assets_file:
        assets = yaml.load(assets_file)

    assets = [{'name': name, 'uri': uri} for name, uri in assets.items()]

    return json.dumps(assets)
