.. tmp store for the merge of Quickstart and Development Quickstart

Development Quickstart Guide
============================

This guide is intended to get you started on developing Visplay as quickly as
possible.

Dependencies
------------

Make sure that you have the following installed and available on your system's
``PATH``.

The following dependencies are required for all work on any part of Visplay.

- Python 3 (probably something like ``python3`` in your package manager)
- Pip 3 (probably something like ``python3-pip`` in your package manager)
- ``youtube-dl`` (depends on your package manager)

Visplay Server and Visplay Renderer
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you plan to work on **Visplay Server** or **Visplay Renderer**, these
additional dependencies are required.

- mpv ``libmpv`` and ``mpv`` (depends on your package manager)
- ``youtube-dl`` (depends on your package manager)
- Qt5 devel libs

Ex. Fedora Dependency Install Commands:

.. code:: bash

  # Install RPM Fusion Repos (Required For MPV and QT5 Devel packages)
  $ sudo dnf install -y \
    https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \ 
    https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
  
  # Install packages
  $ sudo dnf install python3 python3-pip libmpv mpv youtube-dl \
    qt5-qtwebengine-devel qt5-qtwebengine qt5-qtwebengine-devel \
    qt5-qtwebengine mingw64-qt5-qmake mingw32-qt5-qmake qhttpengine \
    qhttpengine-devel qt5-qtwebkit qt5-qtwebkit-devel qt5-qtsvg-devel \
    mpv-libs-devel

.. Ex. Ubuntu install command

Ex. Arch Dependency Install:

.. code:: bash

  # Install packages from Arch Official Repos
  $ sudo pacman -S python python-pip mpv youtube-dl \
    qt5-base qt5-webkit qt5-svg

  # Install packages from AUR
  $ <aur-helper> -S qhttpengine

Visplay Configuration Generator
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you plan to work on the **Configuration Generator**, these additional
dependencies are required:

- ``nodejs`` and ``npm`` (to compile the frontend)

Installation
------------

Clone the repository and install the Python dependencies::

    $ git clone https://gitlab.com/ColoradoSchoolOfMines/visplay.git
    $ cd visplay
    $ pip3 install --user -r requirements.txt

Then follow the instructions for whatever components you want to work on

Visplay Server
^^^^^^^^^^^^^^

1. Install the dependencies::

      $ pip3 install -e . --user

   .. note::

      If you have already installed, add ``--update`` to the ``pip install``
      command.

2. Run ``visplay``. It will prompt you to create the `Default Configuration`_.

Visplay Renderer
^^^^^^^^^^^^^^^^

Run the build script::

    $ cd visplayrenderer
    $ ./build.sh

Visplay Configuration Generator
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Just let NPM handle the installation::

    $ cd config-generator
    $ npm install

Running Visplay
---------------

- **Visplay Server**

  Run the ``visplay`` command.

- **Visplay Renderer**

  Run the ``build/release/bin/visplayrenderer`` command

  Example curl command for visplayrenderer::

      $ curl -H "Content-Type: application/json" -X POST -d '{"path":"https://www.youtube.com/watch?v=gs415cKPASA"}' http://localhost:25565/play

- **Visplay Configuration Generator**

  Run ``npm start`` from the ``config-generator`` directory.

Configuration
-------------

By default, Visplay Server and Visplay Configuration Generator look in
``$XDG_CONFIG_HOME``, and ``$HOME/.config`` for ``visplay/visplay.yaml``.  This
is the main local configuration and points to any number of ``assets.yaml``
files and ``playlists.yaml`` files (whether local or remote).

You can use the ``-c`` or ``--config`` parameters to choose what configuration
file to use.

Default Configuration
^^^^^^^^^^^^^^^^^^^^^

To initialize the default configuration, run ``visplay`` without existing
configuration files. It will ask you if you want to generate the default ones.

If you want to override your existing configuration, delete your existing
configuration files and run ``visplay``.
